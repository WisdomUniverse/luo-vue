// initial state
const state = {
  session_id: '123',
  userName: '',
  userNo: '',
  userImage: ''
}

const getters = {}

const actions = {}

const mutations = {
  getSession_id(state, value) {
    state.session_id = value
  },
  getUserName(state, value) {
    state.userName = value
  },
  getUserNo(state, value) {
    state.userNo = value
  },
  getUserImage(state, value) {
    state.userImage = value
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
