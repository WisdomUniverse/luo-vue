import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

// 路由相关
import vueRouter from 'vue-router'
Vue.use(vueRouter)
import router from './router.js'

// 引入store
import store from './store/index.js'
// 引入JavaScript 实用工具库
import lodash from 'lodash'

// 引入ant组件
import Antd  from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
Vue.use(Antd)
Vue.config.productionTip = false;

// 授权相关
import getUserInfo from './utils/auth.js'
Vue.prototype.$GetUserInfo = getUserInfo
Vue.prototype.$_ = lodash


/* eslint-disable no-new */
new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
