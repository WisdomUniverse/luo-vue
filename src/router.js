import VueRouter from 'vue-router'
import {
  getToken
} from '@/utils/auth'

// 导入对应的路由组件
import login from './view/login/login.vue';
import home from './view/home/home.vue';
import study from './view/study/study.vue';
import money from './view/money/money.vue';
import life from './view/life/life.vue';
import index from './view/index/index.vue';

// 3. 创建路由对象
var router = new VueRouter({
  routes: [ // 配置路由规则
    {
      path: '/login',
      name: '登录',
      component: login,
      meta: {
        title: '登录'
      }
    },
    {
      path: '/home',
      name: '未来主页',
      component: home,
      meta: {
        title: '未来主页'
      },
      children: [
        {
          path: '/index',
          name: '首页',
          component: index,
          meta: {
            title: '首页'
          }
        },
        {
          path: '/study',
          name: '学习',
          component: study,
          meta: {
            title: '学习'
          }
        },
        {
          path: '/money',
          name: '理财',
          component: money,
          meta: {
            title: '理财'
          }
        },
        {
          path: '/life',
          name: '生活',
          component: life,
          meta: {
            title: '生活'
          }
        },
      ]
    }
  ],
  linkActiveClass: '' // 覆盖默认的路由高亮类 默认的叫 router-link-active
})

// 路由全局前置守卫
router.beforeEach(async (to, from, next) => {
  const token = getToken()
  window.console.log('token:' + token)
  if (token && token !== 'undefined') {
    next()
  } else {
    if (to.path === '/login') {
      // 自动登录判断
      // next({ path: '/' })
      next()
    } else {
      next(`/login`)
    }
  }
})
// 把路由对象暴露出去
export default router
