import axios from 'axios'
import {
  message
} from 'ant-design-vue'
import {
  getToken
} from '@/utils/auth'
import router from '../router'

//或者设置 baseURL
//axios.defaults.baseURL='http://localhost:8082';

// axios默认配置
const requestAxios = axios.create({
  baseURL: 'http://localhost:8082', // url = base url + request url
  timeout: 20000 // 请求延时
})

// 整理数据
requestAxios.defaults.transformRequest = function(data) {
  data = JSON.stringify(data)
  console.log('req请求数据：', data)
  return data
}

// 路由请求拦截
requestAxios.interceptors.request.use(
  config => {
    // 当前路由地址
    console.log("当前路由：", router.history.current.path)
    config.headers['Content-Type'] = 'application/json;charset=UTF-8';
    const token = getToken()
    if (token) {
      config.headers.Authorization = 'Bearer ' + token
    }
    console.log("config===>", config)
    return config
  },
  error => {
    return Promise.reject(error.response)
  })

// 路由响应拦截
requestAxios.interceptors.response.use(response => {
  console.log("----------------");
    console.log('response', response)
    /* const {
      config,
      data,
      headers
    } = response
    data.success = data.code === 0
    if (!data.success && !config.noErrorMsg && data.msg) {
      return message.error(data.msg)
    } */
    return response
  },
  async error => {
    // console.log('error', error)
    // message.error(error.message || '请求出错，请稍后重试！')
    // return Promise.reject(error.response)

    console.log(error)
    const res = {
      success: false,
      code: 0,
      msg: ''
    }
    /* if (error ? .response) {
      console.log(error)
      const {
        data,
        status
      } = error.response
      if (_.isNumber(status)) {
        res.code = status
      }
      if (_.isPlainObject(data)) {
        _.merge(res, data)
      } else if (_.isString(data)) {
        res.msg = data || error.response.statusText
      }
    } */

    // 刷新令牌
    const {
      code
    } = res
    if (code === 401) {
      router.push('/login')
      return res
    }

    // 错误消息
    if (!config.noErrorMsg && res.msg) {
      const duration = config.msgDuration >= 0 ? config.msgDuration : 3000
      this.$message.error({
        message: res.msg,
        duration: duration
      })
    }

    return res
  })
export default requestAxios
