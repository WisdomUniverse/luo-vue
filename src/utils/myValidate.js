//验证手机号
export function isPhone(rule, value, callback) {
    if (!value) {
        return callback(new Error('请输入手机号'));
    }
    let pattern = /^1[34578]\d{9}$/;
    if(pattern.test(value)){
        return callback()
    }
    return callback(new Error('输入的手机号错误'))
}
