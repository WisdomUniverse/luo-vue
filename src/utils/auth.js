const tokenKey = 'token'
const userInfoKey = 'userInfo'

export function getToken() {
  return window.sessionStorage.getItem(tokenKey)
}

export function setToken(token) {
  return window.sessionStorage.setItem(tokenKey, token)
}

export function removeToken() {
  return window.sessionStorage.removeItem(tokenKey)
}

var getUserInfo = function() {
  return window.sessionStorage.getItem(userInfoKey)
}
export default getUserInfo

export function setUserInfo(userInfo) {
  return window.sessionStorage.setItem(userInfoKey, userInfo)
}

export function removeUserInfo() {
  return window.sessionStorage.removeItem(userInfoKey)
}
