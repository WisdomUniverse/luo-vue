import request from '@/utils/request'

/**
 * 登录
 */
export const toLogin = (params, config = {}) => {
  return request.post('/luo/login', params, config)
}

export const getLoginInfo = (params, config = {}) => {
  return request.get('GetUserInfo', { params: params }, config)
}
