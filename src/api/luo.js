import request from '@/utils/request'

/**
 * 获取天气
 */
export const getWeather = (params, config = {}) => {
  return request.get('/weather', params, config)
}
